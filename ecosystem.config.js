module.exports = {
    apps: [
        {
            name: "activities",
            script: "./dist/index.js",
            watch: true,
            env: {
                "NODE_ENV": "development",
                "TZ":"America/Bogota"
            },
            env_production: {
                "NODE_ENV": "production",
                "TZ":"America/Bogota"
            },
            env_local: {
                "NODE_ENV": "local",
                "TZ":"America/Bogota"
            }
        }
    ]
}